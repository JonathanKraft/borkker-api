<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Post;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\PostRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Normalizer\SerializerHelper;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;


/**
 * @Route("api/")
 */
class PostController
{
    /**
     * @SWG\Get(
     *  description="Get all User's Posts",
     *  tags={"Post"},
     *  path="/api/posts",
     *  @SWG\Response(
     *    response=201,
     *    description="Found all the Posts",
     *    @Model(type=Post::class)
     *  ),
     *  @SWG\Response(
     *    response="default",
     *    description="an ""unexpected"" error"
     *  )
     * )
     * 
     * @Route("posts", methods={"GET"})
     */
    public function findAll(PostRepository $repository, SerializerHelper $serializer, Security $security)
    {
        
        $posts = $security->getUser()->getPosts();

        return JsonResponse::fromJsonString($serializer->serialize($posts));
    }

    /**
     * @SWG\Get(
     *  description="Get a Post",
     *  tags={"Post"},
     *  path="/api/posts/{post}",
     *  @SWG\Parameter(
     *    name="Post",
     *    in="path",
     *    required=true,
     *    type="integer"
     *  ),
     *  @SWG\Response(
     *    response=201,
     *    description="Found a Post",
     *    @Model(type=Post::class)
     *  ),
     *  @SWG\Response(
     *    response="default",
     *    description="an ""unexpected"" error"
     *  )
     * )
     * 
     * @Route("posts/{post}", methods={"GET"}, requirements={"post"="\d+"})
     */
    public function findOne(Post $post, SerializerHelper $serializer, PostRepository $repository)
    {
        $post = $repository->find($post);

        return JsonResponse::fromJsonString($serializer->serialize($post));
    }

    /**
     * @SWG\Post(
     *  description="Post a new Post",
     *  tags={"Post"},
     *  path="/api/posts",
     *  @SWG\Parameter(
     * @Model(type=Post::class),
     *    name="Post",
     *    in="body",
     *    required=true,
     *    type="body"
     *  ),
     *  @SWG\Response(
     *    response=201,
     *    description="Added a Post",
     *    @Model(type=Post::class)
     *  ),
     *  @SWG\Response(
     *    response="default",
     *    description="an ""unexpected"" error"
     *  )
     * )
     * 
     * @Route("posts", methods={"POST"})
     */
    public function addPost(Request $req, SerializerHelper $serializer, EntityManagerInterface $manager, Security $security)
    {
        dump($req->getContent());
        $post = $serializer->deserialize(
            $req->getContent(),
            Post::class
        );

        dump($security->getUser());
        dump($post);
        dump($serializer->serialize($post));

        $post->setUser($security->getUser());
        $manager->persist($post);
        $manager->flush();

        return JsonResponse::fromJsonString($serializer->serialize($post));
    }

    /**
     * @SWG\Patch(
     *  description="Modify an existing Post  ",
     *  tags={"Post"},
     *  path="/api/posts/{post}",
     *  @SWG\Parameter(
     *    @Model(type=Post::class),
     *    name="post",
     *    in="body",
     *    required=true,
     *    type="body"
     *  ),
     *  @SWG\Response(
     *    response=200,
     *    description="Modified the Post ",
     *    @Model(type=Post::class)
     *  ),
     *  @SWG\Response(
     *    response="default",
     *    description="an ""unexpected"" error"
     *  )
     * )
     * 
     * @Route("posts/{post}", methods={"PATCH"}, requirements={"post"="\d+"})
     */
    public function updatePost(Request $req, Post $post, SerializerHelper $serializer, EntityManagerInterface $manager, Security $security)
    {
        $user = $security->getUser();
        if (in_array($post, $user->getPosts())){
            $update = $serializer->deserializePopulate(
                $req->getContent(),
                Post::class,
                $post
            );
            $manager->persist($post);
            $manager->flush();
    
            return new Response('', 204);
        }
        return new Response('Forbidden', 403);
    }

    /**
     * @SWG\Delete(
     *  description="Delete a Post",
     *  tags={"Post"},
     *  path="/api/posts/{post}",
     *  @SWG\Response(
     *    response=204,
     *    description="The Posts has been removed"
     *  ),
     *  @SWG\Response(
     *    response="default",
     *    description="an ""unexpected"" error"
     *  )
     * )
     * 
     * @Route("posts/{post}", methods={"DELETE"}, requirements={"post"="\d+"})
     */
    public function deleteOne(Post $post, EntityManagerInterface $manager, Security $security)
    {
        $user = $security->getUser();
        if (in_array($post, $user->getPosts())){
            $manager->remove($post);
            $manager->flush();
    
            return new Response('', 204);
        }
        return new Response('Forbidden', 403);
    }

    /**
     * @SWG\Post(
     *  description="Post a new Rebork",
     *  tags={"Post"},
     *  path="/api/posts/rebork/{reborkSource}",
     *  @SWG\Parameter(
     * @Model(type=Post::class),
     *    name="Post",
     *    in="body",
     *    required=true,
     *    type="body"
     *  ),
     *  @SWG\Response(
     *    response=201,
     *    description="Added a Rebork",
     *    @Model(type=Post::class)
     *  ),
     *  @SWG\Response(
     *    response="default",
     *    description="an ""unexpected"" error"
     *  )
     * )
     * 
     * @Route("posts/rebork/{reborkSource}", methods={"POST"}, requirements={
     * "rebork"="rebork",
     * "post"="\d+"
     * })
     */
    public function addRebork(Request $req, Post $reborkSource, SerializerHelper $serializer, EntityManagerInterface $manager, Security $security)
    {
        $post = $serializer->deserialize(
            $req->getContent(),
            Post::class
        );
        $post->setUser($security->getUser());
        $post->setReborkSource($reborkSource);
        $manager->persist($post);

        $reborkSource->addReborkk($post);
        $manager->persist($reborkSource);

        $manager->flush();

        return JsonResponse::fromJsonString($serializer->serialize($post));
    }

    /**
     * @SWG\Post(
     *  description="Post a new Reply",
     *  tags={"Post"},
     *  path="/api/posts/reply/{post}",
     *  @SWG\Parameter(
     * @Model(type=Post::class),
     *    name="Post",
     *    in="body",
     *    required=true,
     *    type="body"
     *  ),
     *  @SWG\Response(
     *    response=201,
     *    description="Added a Reply",
     *    @Model(type=Post::class)
     *  ),
     *  @SWG\Response(
     *    response="default",
     *    description="an ""unexpected"" error"
     *  )
     * )
     * 
     * @Route("posts/reply/{post}", methods={"POST"}, requirements={
     * "rebork"="rebork",
     * "post"="\d+"
     * })
     */
    public function addReply(Request $req, Post $post, SerializerHelper $serializer, EntityManagerInterface $manager, Security $security)
    {
        $reply = $serializer->deserialize(
            $req->getContent(),
            Post::class
        );
        $reply->setUser($security->getUser());
        $reply->setRepliedTo($post);
        $manager->persist($reply);

        $post->addReply($reply);
        
        $manager->persist($post);
        $manager->flush();

        return JsonResponse::fromJsonString($serializer->serialize($reply));
    }
}
