<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Post;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\SqlPostRepo;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * @Route("api/")
 */
class SqlPostController
{
  /**
   * @Route("sqlposts", methods={"GET"})
   */
  public function findAll(SerializerInterface $serializer)
  {
    $repository = new SqlPostRepo();
    $posts = $repository->findAll();

    return JsonResponse::fromJsonString($serializer->serialize(
      $posts,
      'json',
      [
        ObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object) {
          return $object->getId();
        },
        AbstractNormalizer::IGNORED_ATTRIBUTES => ['password']
      ]
    ));
  }

  /**
   * @Route("sqlposts/{post}", methods={"GET"}, requirements={"post"="\d+"})
   */
  public function findOne(Post $post, SerializerInterface $serializer)
  {
    $repository = new SqlPostRepo();

    $post = $repository->find($post->getId());

    return JsonResponse::fromJsonString($serializer->serialize(
      $post,
      'json',
      [
        ObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object) {
          return $object->getId();
        },
        AbstractNormalizer::IGNORED_ATTRIBUTES => ['password']
      ]
    ));
  }

  /**
   * @Route("sqlposts", methods={"POST"})
   */
  public function addPost(Request $req, SerializerInterface $serializer, EntityManagerInterface $manager, Security $security)
  {
    $post = $serializer->deserialize(
      $req->getContent(),
      Post::class,
      'json'
    );
    $post->setUser($security->getUser());
    $manager->persist($post);
    $manager->flush();

    return JsonResponse::fromJsonString($serializer->serialize(
      $post,
      'json',
      [
        ObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object) {
          return $object->getId();
        },
        AbstractNormalizer::IGNORED_ATTRIBUTES => ['password']
      ]
    ));
  }

  /**
   * @Route("sqlposts/{post}", methods={"PATCH"}, requirements={"post"="\d+"})
   */
  public function updatePost(Request $req, Post $post, SerializerInterface $serializer, EntityManagerInterface $manager)
  {
    $update = $serializer->deserialize(
      $req->getContent(),
      Post::class,
      'json'
    );

    $post->setContent($update->getContent());
    $post->setFile($update->getFile());

    $manager->persist($post);
    $manager->flush();

    return new Response('', 204);
  }

  /**
   * @Route("sqlposts/{post}", methods={"DELETE"}, requirements={"post"="\d+"})
   */
  public function deleteOne(Post $post, EntityManagerInterface $manager)
  {
    $manager->remove($post);
    $manager->flush();

    return new Response('', 204);
  }

  /**
   * @Route("sqlposts/rebork/{reborkSource}", methods={"POST"}, requirements={
   * "rebork"="rebork",
   * "post"="\d+"
   * })
   */
  public function addRebork(Request $req, Post $reborkSource, SerializerInterface $serializer, EntityManagerInterface $manager, Security $security)
  {
    $post = $serializer->deserialize(
      $req->getContent(),
      Post::class,
      'json'
    );
    $post->setUser($security->getUser());
    $post->setReborkSource($reborkSource);
    $manager->persist($post);

    $reborkSource->addReborkk($post);
    $manager->persist($reborkSource);

    $manager->flush();

    return JsonResponse::fromJsonString($serializer->serialize(
      $post,
      'json',
      [
        ObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object) {
          return $object->getId();
        },
        AbstractNormalizer::IGNORED_ATTRIBUTES => ['password']
      ]
    ));
  }

  /**
   * @Route("sqlposts/reply/{post}", methods={"POST"}, requirements={
   * "rebork"="rebork",
   * "post"="\d+"
   * })
   */
  public function addReply(Request $req, Post $post, SerializerInterface $serializer, EntityManagerInterface $manager, Security $security)
  {
    $reply = $serializer->deserialize(
      $req->getContent(),
      Post::class,
      'json'
    );
    $reply->setUser($security->getUser());
    $reply->setRepliedTo($post);
    $manager->persist($reply);

    $post->addReply($reply);
    $manager->persist($post);

    $manager->flush();

    return JsonResponse::fromJsonString($serializer->serialize(
      $reply,
      'json',
      [
        ObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object) {
          return $object->getId();
        },
        AbstractNormalizer::IGNORED_ATTRIBUTES => ['password']
      ]
    ));
  }
}
