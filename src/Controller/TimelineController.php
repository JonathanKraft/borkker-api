<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Timeline;
use App\Entity\User;
use App\Repository\TimelineRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use App\Normalizer\SerializerHelper;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\Security\Core\Security;

/**
 * @Route("api/")
 */
class TimelineController
{
    /**
     * @SWG\Get(
     *  description="Get all the User's Timelines",
     *  tags={"Timeline"},
     *  path="/api/timeline",
     *  @SWG\Response(
     *    response=201,
     *    description="Found all the Timelines",
     *    @Model(type=Timeline::class)
     *  ),
     *  @SWG\Response(
     *    response="default",
     *    description="an ""unexpected"" error"
     *  )
     * )
     * 
     * @Route("timelines", methods={"GET"})
     */
    public function findAll(TimelineRepository $repository, SerializerHelper $serializer, Security $security)
    {
        $timelines = $security->getUser()->getTimelines();

        return JsonResponse::fromJsonString($serializer->serialize($timelines));
    }

    /**
     * @SWG\Get(
     *  description="Get a Timeline",
     *  tags={"Timeline"},
     *  path="/api/timeline/{timeline}",
     *  @SWG\Parameter(
     *    name="Timeline",
     *    in="path",
     *    required=true,
     *    type="integer"
     *  ),
     *  @SWG\Response(
     *    response=201,
     *    description="Found a Timeline",
     *    @Model(type=Timeline::class)
     *  ),
     *  @SWG\Response(
     *    response="default",
     *    description="an ""unexpected"" error"
     *  )
     * )
     * 
     * @Route("timelines/{timeline}", methods={"GET"})
     */
    public function findOne(Timeline $timeline, SerializerHelper $serializer, TimelineRepository $repository, Security $security)
    {
        if (in_array($timeline, $security->getUser()->getTimelines())) {
            return JsonResponse::fromJsonString($serializer->serialize($timeline));
        }
        return new Response('Forbidden', 403);
    }

    /**
     * @SWG\Post(
     *  description="Add a new Timeline",
     *  tags={"Timeline"},
     *  path="/api/timeline",
     *  @SWG\Parameter(
     * @Model(type=Timeline::class),
     *    name="Timeline",
     *    in="body",
     *    required=true,
     *    type="body"
     *  ),
     *  @SWG\Response(
     *    response=201,
     *    description="Added a Timeline",
     *    @Model(type=Timeline::class)
     *  ),
     *  @SWG\Response(
     *    response="default",
     *    description="an ""unexpected"" error"
     *  )
     * )
     * 
     * @Route("timelines", methods={"POST"})
     */
    public function addTimeline(Request $req, Security $security, SerializerHelper $serializer, EntityManagerInterface $manager)
    {
        $timeline = $serializer->deserialize(
            $req->getContent(),
            Timeline::class
        );
        $timeline->setUser($security->getUser());
        $manager->persist($timeline);
        $manager->flush();

        return JsonResponse::fromJsonString($serializer->serialize($timeline));
    }

    /**
     * @SWG\Put(
     *  description="Modify an existing Timeline  ",
     *  tags={"Timeline"},
     *  path="/api/timeline/{timeline}",
     *  @SWG\Parameter(
     *    @Model(type=Timeline::class),
     *    name="timeline",
     *    in="body",
     *    required=true,
     *    type="body"
     *  ),
     *  @SWG\Response(
     *    response=200,
     *    description="Modified the Timeline ",
     *    @Model(type=Timeline::class)
     *  ),
     *  @SWG\Response(
     *    response="default",
     *    description="an ""unexpected"" error"
     *  )
     * )
     * 
     * @Route("timelines/{timeline}", methods={"PUT"})
     */
    public function updateTimeline(Request $req, Timeline $timeline, SerializerHelper $serializer, EntityManagerInterface $manager, Security $security)
    {
        if (in_array($timeline, $security->getUser()->getTimelines())) {
            $update = $serializer->deserialize(
                $req->getContent(),
                Timeline::class
            );
    
            $timeline->setName($update->getName());
    
            $manager->persist($timeline);
            $manager->flush();
    
            return new Response('', 204);
        }
        return new Response('Forbidden', 403);
    }

    /**
     * @SWG\Delete(
     *  description="Delete a Timeline",
     *  tags={"Timeline"},
     *  path="/api/timeline/{timeline}",
     *  @SWG\Response(
     *    response=204,
     *    description="The Timelines has been removed"
     *  ),
     *  @SWG\Response(
     *    response="default",
     *    description="an ""unexpected"" error"
     *  )
     * )
     * 
     * @Route("timelines/{timeline}", methods={"DELETE"})
     */
    public function deleteOne(Timeline $timeline, EntityManagerInterface $manager, Security $security)
    {
        if (in_array($timeline, $security->getUser()->getTimelines())) {
            $manager->remove($timeline);
            $manager->flush();
    
            return new Response('', 204);
        }
        return new Response('', 403);
    }

    /**
     * @SWG\Put(
     *  description="Add a user to a Timeline",
     *  tags={"Timeline"},
     *  path="/api/timelines/{timelines}/add-user/{user}",
     *  @SWG\Parameter(
     * @Model(type=Timeline::class),
     *    name="Timeline",
     *    in="body",
     *    required=true,
     *    type="body"
     *  ),
     *  @SWG\Response(
     *    response=201,
     *    description="Added user to Timeline",
     *    @Model(type=Timeline::class)
     *  ),
     *  @SWG\Response(
     *    response="default",
     *    description="an ""unexpected"" error"
     *  )
     * )
     * 
     * @Route("timelines/{timeline}/add-user/{user}", methods={"PUT"}, requirements={
     * "timeline"="\d+",
     * "user"="\d+"
     * }))
     */
    public function addUserToTimeline(User $user, Timeline $timeline, SerializerHelper $serializer, EntityManagerInterface $manager, Security $security)
    {
        if(in_array($timeline, $security->getUser()->getTimelines())){
            $timeline->addFollowedUser($user);
            $manager->persist($timeline);
            $manager->flush();
    
            return JsonResponse::fromJsonString($serializer->serialize($timeline));
        }
        return new Response('Forbidden', 403);
    }

    /**
     * @SWG\Put(
     *  description="Removes a user to a Timeline",
     *  tags={"Timeline"},
     *  path="/api/timelines/{timeline}/remove-user/{user}",
     *  @SWG\Parameter(
     * @Model(type=Timeline::class),
     *    name="Timeline",
     *    in="body",
     *    required=true,
     *    type="body"
     *  ),
     *  @SWG\Response(
     *    response=201,
     *    description="Removed user to Timeline",
     *    @Model(type=Timeline::class)
     *  ),
     *  @SWG\Response(
     *    response="default",
     *    description="an ""unexpected"" error"
     *  )
     * )
     * 
     * @Route("timelines/{timeline}/remove-user/{user}", methods={"PUT"}, requirements={
     * "timeline"="\d+",
     * "remove-user"="remove-user",
     * "user"="\d+"
     * }))
     */
    public function removeUserFromTimeline(User $user, Timeline $timeline, SerializerHelper $serializer, EntityManagerInterface $manager, Security $security)
    {
        if(in_array($timeline, $security->getUser()->getTimelines())){
            $timeline->addFollowedUser($user);
            $manager->persist($timeline);
            $manager->flush();
    
            return JsonResponse::fromJsonString($serializer->serialize($timeline));
        }
        return new Response('Forbidden', 403);
    }
}
