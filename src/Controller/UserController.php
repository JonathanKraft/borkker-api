<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use App\Normalizer\SerializerHelper;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\Security\Core\Security;

/**
 * @Route("api/")
 */
class UserController
{
    /**
     * @SWG\Get(
     *  description="Get all the Users",
     *  tags={"User"},
     *  path="/api/user",
     *  @SWG\Response(
     *    response=201,
     *    description="Found all the Users",
     *    @Model(type=User::class)
     *  ),
     *  @SWG\Response(
     *    response="default",
     *    description="an ""unexpected"" error"
     *  )
     * )
     * 
     * @Route("users", methods={"GET"})
     */
    public function findAll(UserRepository $repository, SerializerHelper $serializer)
    {
        $users = $repository->findAll();

        return JsonResponse::fromJsonString($serializer->serialize(
            $users
        ));
    }

    /**
     * @SWG\Get(
     *  description="Get a User",
     *  tags={"User"},
     *  path="/api/user/{id}",
     *  @SWG\Parameter(
     *    name="User",
     *    in="path",
     *    required=true,
     *    type="integer"
     *  ),
     *  @SWG\Response(
     *    response=201,
     *    description="Found a User",
     *    @Model(type=User::class)
     *  ),
     *  @SWG\Response(
     *    response="default",
     *    description="an ""unexpected"" error"
     *  )
     * )
     *
     * @Route("users/{user}", methods={"GET"}, requirements={"id"="\d+"})
     */
    public function findOne(User $user, SerializerHelper $serializer, UserRepository $repository)
    {
        return JsonResponse::fromJsonString($serializer->serialize($user));
    }

    /**
     * @SWG\Post(
     *  description="Post a new User",
     *  tags={"User"},
     *  path="/api/user",
     *  @SWG\Parameter(
     * @Model(type=User::class),
     *    name="User",
     *    in="body",
     *    required=true,
     *    type="body"
     *  ),
     *  @SWG\Response(
     *    response=201,
     *    description="Added a User",
     *    @Model(type=User::class)
     *  ),
     *  @SWG\Response(
     *    response="default",
     *    description="an ""unexpected"" error"
     *  )
     * )
     *
     * @Route("users", methods={"POST"})
     */
    public function addUser(Request $req, SerializerHelper $serializer, EntityManagerInterface $manager, UserPasswordEncoderInterface $encoder)
    {
        $user = $serializer->deserialize(
            $req->getContent(),
            User::class
        );
        $user->setPassword($encoder->encodePassword($user, $user->getPassword()));
        $manager->persist($user);
        $manager->flush();

        return JsonResponse::fromJsonString($serializer->serialize(
            $user
        ));
    }

    /**
     * @SWG\Put(
     *  description="Modify an existing User  ",
     *  tags={"User"},
     *  path="/api/user/{id}",
     *  @SWG\Parameter(
     *    @Model(type=User::class),
     *    name="user",
     *    in="body",
     *    required=true,
     *    type="body"
     *  ),
     *  @SWG\Response(
     *    response=200,
     *    description="Modified the User ",
     *    @Model(type=User::class)
     *  ),
     *  @SWG\Response(
     *    response="default",
     *    description="an ""unexpected"" error"
     *  )
     * )
     *
     * @Route("users/{user}", methods={"PUT"})
     */
    public function updateUser(Request $req, User $user, SerializerHelper $serializer, EntityManagerInterface $manager, UserPasswordEncoderInterface $encoder, Security $security)
    {
        if ($user === $security->getUser()) {
            $update = $serializer->deserializePopulate(
                $req->getContent(),
                User::class,
                $user
            );
            $update->setPassword($encoder->encodePassword($update, $update->getPassword()));

            $manager->persist($user);
            $manager->flush();
    
            return new Response('', 204);
        }
        return new Response ('Forbidden', 403);
    }

    /**
     * @SWG\Delete(
     *  description="Delete a User",
     *  tags={"User"},
     *  path="/api/user/{id}",
     *  @SWG\Response(
     *    response=204,
     *    description="The Users has been removed"
     *  ),
     *  @SWG\Response(
     *    response="default",
     *    description="an ""unexpected"" error"
     *  )
     * )
     *
     * @Route("users/{user}", methods={"DELETE"})
     */
    public function deleteOne(User $user, EntityManagerInterface $manager, Security $security)
    {
        if ($user === $security->getUser()) {
            $manager->remove($user);
            $manager->flush();
    
            return new Response('', 204);
        }
        return new Response('Forbidden', 403);
    }

    /**
     * @SWG\Get(
     *  description="Get a the logged User",
     *  tags={"User"},
     *  path="/api/users/get-logged",
     *  @SWG\Parameter(
     *    name="User",
     *    in="path",
     *    required=true,
     *    type="integer"
     *  ),
     *  @SWG\Response(
     *    response=201,
     *    description="Found a the logged User",
     *    @Model(type=User::class)
     *  ),
     *  @SWG\Response(
     *    response="default",
     *    description="an ""unexpected"" error"
     *  )
     * )
     *
     * @Route("users/get-logged", methods={"GET"}, requirements={"get-logged"="get-logged"})
     */
    public function getLogged(Security $security, SerializerHelper $serializer)
    {
        return JsonResponse::fromJsonString($serializer->serialize($security->getUser()));
    }
}
