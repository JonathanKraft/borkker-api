<?php

namespace App\Controller\admin;

use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Post;
use App\Normalizer\SerializerHelper;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;

/**
 * @Route("api/admin/")
 */
class AdminPostController
{
    /**
     * @SWG\Put(
     *  description="Modify an existing Post  ",
     *  tags={"Post"},
     *  path="/api/admin/posts/{post}",
     *  @SWG\Parameter(
     *    @Model(type=Post::class),
     *    name="post",
     *    in="body",
     *    required=true,
     *    type="body"
     *  ),
     *  @SWG\Response(
     *    response=200,
     *    description="Modified the Post ",
     *    @Model(type=Post::class)
     *  ),
     *  @SWG\Response(
     *    response="default",
     *    description="an ""unexpected"" error"
     *  )
     * )
     * 
     * @Route("posts/{post}", methods={"PATCH"}, requirements={"post"="\d+"})
     */
    public function updatePost(Request $req, Post $post, SerializerHelper $serializer, EntityManagerInterface $manager)
    {
        $update = $serializer->deserialize(
            $req->getContent(),
            Post::class,
            'json'
        );

        $post->setContent($update->getContent());
        $post->setFile($update->getFile());

        $manager->persist($post);
        $manager->flush();

        return new Response('', 204);
    }

    /**
     * @SWG\Delete(
     *  description="Delete a Post",
     *  tags={"Post"},
     *  path="/api/admin/posts/{post}",
     *  @SWG\Response(
     *    response=204,
     *    description="The Post has been removed"
     *  ),
     *  @SWG\Response(
     *    response="default",
     *    description="an ""unexpected"" error"
     *  )
     * )
     * 
     * @Route("posts/{post}", methods={"DELETE"}, requirements={"post"="\d+"})
     */
    public function deleteOne(Post $post, EntityManagerInterface $manager)
    {
        $manager->remove($post);
        $manager->flush();

        return new Response('', 204);
    }
}
