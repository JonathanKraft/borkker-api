<?php

namespace App\Controller\admin;

use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Timeline;
use App\Repository\TimelineRepository;
use App\Normalizer\SerializerHelper;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;

/**
 * @Route("api/admin/")
 */
class AdminTimelineController
{
    /**
     * @SWG\Get(
     *  description="Get all the Timelines",
     *  tags={"Timeline"},
     *  path="/api/admin/timeline",
     *  @SWG\Response(
     *    response=201,
     *    description="Found all the Timelines",
     *    @Model(type=Timeline::class)
     *  ),
     *  @SWG\Response(
     *    response="default",
     *    description="an ""unexpected"" error"
     *  )
     * )
     * 
     * @Route("timelines", methods={"GET"})
     */
    public function findAll(TimelineRepository $repository, SerializerHelper $serializer)
    {
        $timelines = $repository->findAll();

        return JsonResponse::fromJsonString($serializer->serialize(
            $timelines,
            'json'
        ));
    }

    /**
     * @SWG\Get(
     *  description="Get a Timeline",
     *  tags={"Timeline"},
     *  path="/api/admin/timeline/{timeline}",
     *  @SWG\Parameter(
     *    name="Timeline",
     *    in="path",
     *    required=true,
     *    type="integer"
     *  ),
     *  @SWG\Response(
     *    response=201,
     *    description="Found a Timeline",
     *    @Model(type=Timeline::class)
     *  ),
     *  @SWG\Response(
     *    response="default",
     *    description="an ""unexpected"" error"
     *  )
     * )
     * 
     * @Route("timelines/{timeline}", methods={"GET"})
     */
    public function findOne(int $id, SerializerHelper $serializer, TimelineRepository $repository)
    {
        $timeline = $repository->find($id);

        return JsonResponse::fromJsonString($serializer->serialize(
            $timeline,
            'json'
        ));
    }

    /**
     * @SWG\Put(
     *  description="Modify an existing Timeline  ",
     *  tags={"Timeline"},
     *  path="/api/admin/timeline/{timeline}",
     *  @SWG\Parameter(
     *    @Model(type=Timeline::class),
     *    name="timeline",
     *    in="body",
     *    required=true,
     *    type="body"
     *  ),
     *  @SWG\Response(
     *    response=200,
     *    description="Modified the Timeline ",
     *    @Model(type=Timeline::class)
     *  ),
     *  @SWG\Response(
     *    response="default",
     *    description="an ""unexpected"" error"
     *  )
     * )
     * 
     * @Route("timelines/{timeline}", methods={"PUT"})
     */
    public function updateTimeline(Request $req, Timeline $timeline, SerializerHelper $serializer, EntityManagerInterface $manager)
    {
        $update = $serializer->deserializePopulate(
            $req->getContent(),
            Timeline::class,
            $timeline
        );
        $manager->persist($timeline);
        $manager->flush();

        return new Response('', 204);
    }

    /**
     * @SWG\Delete(
     *  description="Delete a Timeline",
     *  tags={"Timeline"},
     *  path="/api/admin/timeline/{timeline}",
     *  @SWG\Response(
     *    response=204,
     *    description="The Timelines has been removed"
     *  ),
     *  @SWG\Response(
     *    response="default",
     *    description="an ""unexpected"" error"
     *  )
     * )
     * 
     * @Route("timelines/{timeline}", methods={"DELETE"})
     */
    public function deleteOne(Timeline $timeline, EntityManagerInterface $manager)
    {
        $manager->remove($timeline);
        $manager->flush();

        return new Response('', 204);
    }
}
