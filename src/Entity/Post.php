<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PostRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Post
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $content;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $file;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $report;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $vote;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="posts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Post", inversedBy="replies")
     */
    private $repliedTo;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Post", mappedBy="repliedTo")
     */
    private $replies;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Post", inversedBy="reborkks")
     */
    private $reborkSource;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Post", mappedBy="reborkSource")
     */
    private $reborkks;

    public function __construct(int $id = null, DateTime $date = null, string $content = null)
    {
        $this->replies = new ArrayCollection();
        $this->reborkks = new ArrayCollection();

        $this->id = $id;
        $this->date = $date;
        $this->content = $content;
    }

    /**
     * Builds a Post from the database's data
     * 
     * @param array Associative array containg the result of a SQL query
     * @return Post Post created from the data from the database
     */
    public static function fromSQL(array $rawData)
    {
        return new User(
            $rawData["id"],
            $rawData["date"],
            $rawData["content"],
            new DateTime($rawData["date"])
        );
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @param \DateTime
     */
    public function setDateValue(): self
    {
        $this->date = new \DateTime();
        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(?string $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getReport(): ?int
    {
        return $this->report;
    }

    public function setReport(?int $report): self
    {
        $this->report = $report;

        return $this;
    }

    public function getVote(): ?int
    {
        return $this->vote;
    }

    public function setVote(int $vote): self
    {
        $this->vote = $vote;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getRepliedTo(): ?self
    {
        return $this->repliedTo;
    }

    public function setRepliedTo(?self $repliedTo): self
    {
        $this->repliedTo = $repliedTo;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getReplies(): Collection
    {
        return $this->replies;
    }

    public function addReply(self $reply): self
    {
        if (!$this->replies->contains($reply)) {
            $this->replies[] = $reply;
            $reply->setRepliedTo($this);
        }

        return $this;
    }

    public function removeReply(self $reply): self
    {
        if ($this->replies->contains($reply)) {
            $this->replies->removeElement($reply);
            // set the owning side to null (unless already changed)
            if ($reply->getRepliedTo() === $this) {
                $reply->setRepliedTo(null);
            }
        }

        return $this;
    }

    public function getReborkSource(): ?self
    {
        return $this->reborkSource;
    }

    public function setReborkSource(?self $reborkSource): self
    {
        $this->reborkSource = $reborkSource;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getReborkks(): Collection
    {
        return $this->reborkks;
    }

    public function addReborkk(self $reborkk): self
    {
        if (!$this->reborkks->contains($reborkk)) {
            $this->reborkks[] = $reborkk;
            $reborkk->setReborkSource($this);
        }

        return $this;
    }

    public function removeReborkk(self $reborkk): self
    {
        if ($this->reborkks->contains($reborkk)) {
            $this->reborkks->removeElement($reborkk);
            // set the owning side to null (unless already changed)
            if ($reborkk->getReborkSource() === $this) {
                $reborkk->setReborkSource(null);
            }
        }

        return $this;
    }
}
