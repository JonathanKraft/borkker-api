<?php

namespace App\Normalizer;

use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Serializer Helper
 * 
 * Wrapper to avoid code duplication on serizlization
 * 
 * Usage example: 
 * $serializer = new SerializerHelper('json');
 * $serializer->serialize($objectToSerialize, ["element", "to", "ignore"]);
 * $serializer->deserialize($rawDataToDeserialize, Entity::class, ["element", "to", "ignore"]);
 * $serializer->deserializePopulate($rawDataToDeserialize, Entity::class, $objectToPopulate, ["element", "to", "ignore"]);
 */
class SerializerHelper
{

    private $format;
    private $defaultAttributes;
    private $serializerInterface;

    public function __construct(String $format = 'json', SerializerInterface $serializerInterface)
    {
        $this->format = $format;
        $this->defaultAttributes = [
            AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true,
            AbstractNormalizer::IGNORED_ATTRIBUTES => ['password', 'salt', 'timezone'],
            AbstractNormalizer::CIRCULAR_REFERENCE_LIMIT => 1,
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object) {
                return $object->getId();
            }
        ];
        $this->serializerInterface = $serializerInterface;
    }

    public function serialize($data, $attributesToIgnore = [])
    {
        $attributes = $this->attributeMerger($attributesToIgnore);
        return $this->serializerInterface->serialize($data, $this->format, $attributes);
    }

    public function deserialize($data, $entity, $attributesToIgnore = [])
    {
        $attributes = $this->attributeMerger($attributesToIgnore);
        dump($data);
        return $this->checkJSON($data) ? $this->serializerInterface->deserialize($data, $entity, $this->format, $attributes) : false;
    }

    public function deserializePopulate($data, $entity, $populate, $attributesToIgnore = [])
    {
        $attributes = $this->attributeMerger($attributesToIgnore, $populate);

        return $this->checkJSON($data) ? $this->serializerInterface->deserialize($data, $entity, $this->format, $attributes) : false;
    }

    private function checkJSON($json)
    {
        // Check JSON validity
        return is_object(json_decode($json));
    }

    private function attributeMerger($attributesToIgnore, $populate = null) 
    {
        $attributes = $this->defaultAttributes;
        $attributes[AbstractNormalizer::IGNORED_ATTRIBUTES] = array_merge($attributes[AbstractNormalizer::IGNORED_ATTRIBUTES], $attributesToIgnore);

        if ($populate !== null) {
            $attributes[AbstractNormalizer::OBJECT_TO_POPULATE] = $populate;
        }
        return $attributes;
    }
}

  // public function update(Request $req, Audit $audit, SerializerHelper $serializer)
  //   {
  //       $update = $serializer->deserializePopulate($req->getContent(), Audit::class, $audit);
  //       $update->setCreator($this->getUser());
  //       $manager = $this->getDoctrine()->getManager();
  //       $manager->persist($update);
  //       $manager->flush();

  //       return JsonResponse::fromJsonString($serializer->serialize($update), 200);

  //   }