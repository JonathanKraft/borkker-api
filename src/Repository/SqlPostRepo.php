<?php

namespace App\Repository;

use App\Entity\Post;

class SqlPostRepo
{

    private $connection;

    public function __construct()
    {
        try {
            $this->connection = new \PDO(
                "mysql:host=" . getenv("DATABASE_URL")
            );

            $this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        } catch (\PDOException $e) {
            dump($e);
        }
    }

    private function fetch(string $query, array $params = [])
    {
        try {
            $query = $this->connection->prepare($query);
            foreach ($params as $param => $value) {
                $query->bindValue($param, $value);
            }
            $query->execute();
            $result = [];
            foreach ($query->fetchAll() as $row) {
                $result[] = Post::fromSQL($row);
            }
            if (count($result) <= 1) {
                return $result[0];
            }
            return $result;
        } catch (\PDOException $e) {
            dump($e);
        }
    }

    public function findAll()
    {
        return $this->fetch("SELECT * FROM post");
    }
    
    public function find(int $id)
    {
        return $this->fetch("SELECT * FROM post WHERE id=:id", [":id" => $id]);
    }

    public function add(Post $post)
    {
        $this->fetch(
            "INSERT INTO post (content, date)  VALUES (:content, :date)",
            [
              ":id" => $post->getId(),
              ":content" => $post->getContent(),
              ":date" => $post->getDate(),
            ]
        );
        $post->id = intval($this->connection->lastInsertId());
        return $post;
    }

    public function update(Post $post)
    {
        return $this->fetch(
            "UPDATE post SET content=:content, date=:date WHERE id=:id",
            [
                ":id" => $post->getId(),
                ":content" => $post->getContent(),
                ":date" => $post->getDate(),
            ]);
    }

    public function delete(int $id){
        return $this->fetch("DELETE FROM post WHERE id=:id",
    [
        ":id" => $id
    ]);
    }
}
